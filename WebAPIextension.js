registerPlugin({
    name: 'HTTP API Extension',
    version: '1.0',
    description: 'This plugin extends HTTP API ',
    author: 'lukaskabc <lukaskabc.tk>',
    vars: []
}, (_, {}) => {
    const event = require('event');
    const media = require('media');
    const engine = require('engine');

    engine.log("test");

    event.on('api:youtube-queue', function(ev){

        engine.log("got request");
        engine.log(JSON.stringify(ev));

        var user = ev.user();

        if(user == null) return {success: false, error: 401}

        engine.log("pex: "+hasQueuePermission(user))

        if(!hasQueuePermission(user)) return {success: false, error: 403}


        let data = ev.data();
        if(data.instanceId != engine.getInstanceID()){

            return {
                success: false,
                error: "no instance id"
            }

        }
        if(data.ytURL == null){

            return {
                success: false,
                error: "no yt url"
            }

        } else if(data.ytURL.toString().trim() == "") {
            return {
                success: false,
                error: "empty url"
            }
        }

        if(data.next != null && data.next == true){

            if(media.playAsNext(data.ytURL)){
                engine.log("enqueue next")
                return {
                    success: true,
                    error: 200
                }
            }

        } else {
            if(media.enqueueYt(data.ytURL)){
                engine.log("enqueue")
                return {
                    success: true,
                    error: 200
                }
            }
        }

        

        return {
            success: false,
            error: "data: "+JSON.stringify(ev)
        };

    });

    const ENQUEUE           = 1 << 13;
    const SKIP_QUEUE        = 1 << 14;
    const ADMIN_QUEUE       = 1 << 15;
    const PLAYBACK          = 1 << 12;
    const START_STOP        = 1 <<  8;
    const EDIT_BOT_SETTINGS = 1 << 16;
    const LOGIN             = 1 <<  0;
    const UPLOAD_FILES      = 1 <<  2;
    const DELETE_FILES      = 1 <<  3;
    const EDIT_FILES        = 1 <<  4;
    const CREATE_AND_DELETE_PLAYLISTS = 1 << 5;
    const EDIT_PLAYLISTS    = 1 <<  7;
    const EDIT_INSTANCES    = 1 << 17;
    const EDIT_USERS        = 1 <<  9;

    function hasQueuePermission(user) {
        // returns true if user has enqueue or admin queue permission
        return ((user.privileges() & ENQUEUE) != 0 || (user.privileges() & ADMIN_QUEUE) != 0)
    }
})