package tk.lukaskabc.sinusbotcontroller

enum class State {
    SUCCESS, FAILED
}

class TStatus(val status: State, val payload: Any) {

    override fun toString(): String{
        var rtrn = "";
        if(status == State.SUCCESS) rtrn += "SUCCESS";
        else rtrn += "FAILED";

        return rtrn +"\n"+ payload.toString();

    }
}