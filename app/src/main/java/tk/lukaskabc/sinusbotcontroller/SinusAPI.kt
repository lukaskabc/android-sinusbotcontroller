package tk.lukaskabc.sinusbotcontroller

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import khttp.responses.Response
import khttp.structures.files.FileLike
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener
import java.io.InputStream
import java.io.Serializable
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.URL
import java.net.UnknownHostException


class SinusAPI(val address: String,val port: Int,val SSL:Boolean = true) : Serializable {

//default bot id c86c1619-8454-40bb-8fdb-ac5b872dea15
    protected var token:String? = null
    protected var headers:Any? = null;

    var activity: MainActivity? = null

    val EOL = "\n";

    var uri:String = "";

    init{

        // ssl init
        if(SSL){
            uri = "https://";
        } else {
            uri = "http://";
        }


        //address analyzation
        var addrs = address;
        addrs.trim('/');

        uri += addrs + ":$port";
        uri += "/api/v1/bot/";

    }

    fun logout() {
        token = null
        uri = ""

    }

    suspend fun login(user: String, password:String, botID:String? = null): TStatus {

        var botid: String? = null;

        if(botID == null) {

            var rslt = DefaultBotID();

            if(rslt.status == State.FAILED) return TStatus(State.FAILED, rslt.payload.toString());

            botid = rslt.payload.toString();

        } else {
            botid = botID;
        }

        try {
            // timeout 3.6 suggested - not great not terrible
            var result = khttp.post(uri + "login", data = mapOf("username" to user, "password" to password, "botId" to botid.toString()), timeout = 10.0);

            if (result.statusCode == 200 && result.jsonObject.get("success").toString().equals("true")) {
                token = result.jsonObject.get("token").toString();
                return TStatus(State.SUCCESS, "success");
            } else {
                return TStatus(State.FAILED, result.statusCode);
            }
        } catch(e: Exception){
            return TStatus(State.FAILED, CatchException(e));
        }
    }

    fun loggedIn() : Boolean {
        return token != null
    }


    suspend fun DefaultBotID(): TStatus {

        var botidreq: Response? = null;
        try {
            botidreq = khttp.get(uri.replace("bot/", "botId"), timeout = 10.0);
            if (botidreq.statusCode != 200) return TStatus(State.FAILED, "BotID request failed with code " + botidreq.statusCode);


            return TStatus(State.SUCCESS, botidreq.jsonObject.get("defaultBotId").toString());
        } catch(e: Exception){
            return TStatus(State.FAILED, CatchException(e));
        }

        //return@async TStatus(State.FAILED, "Something went wrong");
    }

    fun CatchException(e: Any):String {

        when(e){
            is SocketTimeoutException -> return "Connection timeout";
            is UnknownHostException -> return "Check your internet connection"
            is ConnectException -> return "Check your network connection"
        }

        return "Something went wrong"

        /*
        catch (e:SocketTimeoutException) {
            return@async TStatus(State.FAILED, "Connection timeout");
        }catch (e: UnknownHostException) {
            return@async TStatus(State.FAILED, "Check your internet connection");
        }catch (e: ConnectException){
            return@async TStatus(State.FAILED, "Check your network connection");
        } catch (e:Exception){
            e.printStackTrace();
            return@async TStatus(State.FAILED, "Something went wrong");
        }*/
    }

    fun ValidateJson(str:String): Boolean{
        try {
            JSONObject(str)
        } catch (e: JSONException) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                JSONArray(str)
            } catch (ex: JSONException) {
                return false;
            }
        }
        return true;
    }


    suspend fun get(target:String, useSelectedInstance:Boolean = false, instanceID:String? = null): TStatus {

        var instanceId:String? = null;

        if(instanceID != null) instanceId = instanceID
        if(useSelectedInstance) instanceId = activity?.instanceId


        var finalTarget = uri + target.trim('/');

        if(instanceId != null){
            finalTarget = uri + "i/" + instanceId.trim('/') + "/" + target.trim('/');
        }

        try{
            var result = khttp.get(finalTarget, headers=mapOf("Authorization" to "Bearer $token"), timeout = 10.0);

            if(result.statusCode != 200){

                if(result.statusCode == 401){
                    return TStatus(State.FAILED, "Unauthorized");
                }

                return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text );
            }

            if(ValidateJson(result.text)){
                var json = JSONTokener(result.text).nextValue()
                when (json) {
                    is JSONObject -> {
                        return TStatus(State.SUCCESS, result.jsonObject);
                    }
                    is JSONArray -> {
                        return TStatus(State.SUCCESS, result.jsonArray);
                    }
                    else -> {
                        return TStatus(State.FAILED, "invalid json");
                    }
                }
            }

            return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text);
        } catch(e: Exception){
            return TStatus(State.FAILED, CatchException(e));
        }

    }

    suspend fun post(target:String, payload:Map<Any, Any> = mapOf("" to ""), useSelectedInstance:Boolean = false, instanceID:String? = null, ownTarget:Boolean = false): TStatus {

        var instanceId:String? = null;

        if(instanceID != null) instanceId = instanceID
        if(useSelectedInstance) instanceId = activity?.instanceId

        var finalTarget = uri + target.trim('/');

        if(instanceId != null){
            finalTarget = uri + "i/" + instanceId.trim('/') + "/" + target.trim('/');
        }

        if(ownTarget) {
            finalTarget = uri.replace("api/v1/bot/", target.trim('/'));
        }


        try {
            var result = khttp.post(finalTarget, json = payload, headers = mapOf("Authorization" to "Bearer $token"), timeout = 20.0);

            if (result.statusCode != 200) {
                if (result.statusCode == 401) {
                    return TStatus(State.FAILED, "Unauthorized");
                }
                return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text);
            }

            if (ValidateJson(result.text)) {
                var json = JSONTokener(result.text).nextValue()
                when (json) {
                    is JSONObject -> {
                        return TStatus(State.SUCCESS, result.jsonObject);
                    }
                    is JSONArray -> {
                        return TStatus(State.SUCCESS, result.jsonArray);
                    }
                    else -> {
                        return TStatus(State.FAILED, "invalid json");
                    }
                }
            }

            return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text);
        } catch(e: Exception){
            Log.e("error", e.stackTrace.toString())
            e.printStackTrace()
            return TStatus(State.FAILED, CatchException(e));
        }
    }

    suspend fun delete(target:String, payload:Map<Any, Any> = mapOf("" to ""), useSelectedInstance:Boolean = false, instanceID:String? = null): TStatus {

        var instanceId:String? = null;

        if(instanceID != null) instanceId = instanceID
        if(useSelectedInstance) instanceId = activity?.instanceId

        var finalTarget = uri + target.trim('/');

        if(instanceId != null){
            finalTarget = uri + "i/" + instanceId.trim('/') + "/" + target.trim('/');
        }

        try {

            var result = khttp.delete(finalTarget, json = payload, headers = mapOf("Authorization" to "Bearer $token"), timeout = 20.0);

            if (result.statusCode != 200) {
                if (result.statusCode == 401) {
                    return TStatus(State.FAILED, "Unauthorized");
                }
                return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text);
            }

            if (ValidateJson(result.text)) {
                var json = JSONTokener(result.text).nextValue()
                when (json) {
                    is JSONObject -> {
                        return TStatus(State.SUCCESS, result.jsonObject);
                    }
                    is JSONArray -> {
                        return TStatus(State.SUCCESS, result.jsonArray);
                    }
                    else -> {
                        return TStatus(State.FAILED, "invalid json");
                    }
                }
            }

            return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text);
        } catch(e: Exception){
            return TStatus(State.FAILED, CatchException(e));
        }
    }

    suspend fun patch(target:String, payload:Map<Any, Any> = mapOf("" to ""), useSelectedInstance:Boolean = false, instanceID:String? = null): TStatus {

        var instanceId:String? = null;

        if(instanceID != null) instanceId = instanceID
        if(useSelectedInstance) instanceId = activity?.instanceId

        var finalTarget = uri + target.trim('/');

        if(instanceId != null){
            finalTarget = uri + "i/" + instanceId.trim('/') + "/" + target.trim('/');
        }

        try {

            var result = khttp.patch(finalTarget, json = payload, headers = mapOf("Authorization" to "Bearer $token"), timeout = 20.0);

            if (result.statusCode != 200) {
                if (result.statusCode == 401) {
                    return TStatus(State.FAILED, "Unauthorized");
                }
                return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text);
            }

            if (ValidateJson(result.text)) {
                var json = JSONTokener(result.text).nextValue()
                when (json) {
                    is JSONObject -> {
                        return TStatus(State.SUCCESS, result.jsonObject);
                    }
                    is JSONArray -> {
                        return TStatus(State.SUCCESS, result.jsonArray);
                    }
                    else -> {
                        return TStatus(State.FAILED, "invalid json");
                    }
                }
            }

            return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text);
        } catch(e: Exception){
            return TStatus(State.FAILED, CatchException(e));
        }
    }

    suspend fun upload(target:String, files: List<FileLike>, payload:Map<Any, Any> = mapOf("" to ""), useSelectedInstance:Boolean = false, instanceID:String? = null): TStatus  {

        var instanceId:String? = null;

        if(instanceID != null) instanceId = instanceID
        if(useSelectedInstance) instanceId = activity?.instanceId

        var finalTarget = uri + target.trim('/');

        if(instanceId != null){
            finalTarget = uri + "i/" + instanceId.trim('/') + "/" + target.trim('/');
        }

        try {
            var result = khttp.post(finalTarget, json = payload, files = files, headers = mapOf("Authorization" to "Bearer $token"), timeout = 60.0);

            if (result.statusCode != 200) {
                if (result.statusCode == 401) {
                    return TStatus(State.FAILED, "Unauthorized");
                }
                return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text);
            }

            if (ValidateJson(result.text)) {
                var json = JSONTokener(result.text).nextValue()
                when (json) {
                    is JSONObject -> {
                        return TStatus(State.SUCCESS, result.jsonObject);
                    }
                    is JSONArray -> {
                        return TStatus(State.SUCCESS, result.jsonArray);
                    }
                    else -> {
                        return TStatus(State.FAILED, "invalid json");
                    }
                }
            }

            return TStatus(State.FAILED, result.statusCode.toString() + EOL + result.text);
        } catch(e: Exception){
            return TStatus(State.FAILED, CatchException(e));
        }
    }

    suspend fun getThumbnail(fileName: String): TStatus {

        try {

            val stream = URL(uri.replace("/api/v1/bot/", "/cache/" + fileName.trim('/'))).getContent() as InputStream

            return TStatus(State.SUCCESS, BitmapFactory.decodeStream(stream))
        } catch (e: Exception) {
            e.printStackTrace()
            return TStatus(State.FAILED, e.message.toString())
        }


    }

    suspend fun getInstances():TStatus {

        if(!loggedIn()) return TStatus(State.FAILED, "Unauthorized");

        return get("instances");

    }


}