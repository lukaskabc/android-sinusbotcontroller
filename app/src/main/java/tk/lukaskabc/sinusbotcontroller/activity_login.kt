package tk.lukaskabc.sinusbotcontroller

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.security.KeyPairGeneratorSpec
import android.util.Base64
import android.util.Log
import android.view.View
import android.webkit.URLUtil
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.math.BigInteger
import java.nio.charset.Charset
import java.security.AccessController.getContext
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.util.*
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.security.auth.x500.X500Principal
import android.provider.Settings.Secure;
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import java.security.*
import kotlin.system.measureTimeMillis

class activity_login : AppCompatActivity() {

    var LoadingDialog: AlertDialog? = null;

    var context : Context? = null;

    val ks : KeyStore = KeyStore.getInstance("AndroidKeyStore");
    var keyAliases = ArrayList<Any>();


    init {
        ks.load(null);
    }
    //region loginItSelf

    override fun onCreate(savedInstanceState: Bundle?) {

        context = this;



        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(findViewById(R.id.toolbar))



        /* příprava loading dialogu */

        val inflater = layoutInflater

        val customView: View = inflater.inflate(R.layout.loading_dialog, null)

        customView.findViewById<TextView>(R.id.loading_text).text = "Logging in...";

        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setView(customView)
        //builder.setMessage("Logging in...")
        LoadingDialog = builder.create();

        findViewById<CheckBox>(R.id.ssl).setOnCheckedChangeListener { buttonView, isChecked ->

            val port = findViewById<EditText>(R.id.port).text.toString();

            if(isChecked && port != "443") {

                if(port == ""){
                    findViewById<EditText>(R.id.port).setText("443");
                }

                runOnUiThread {
                    Toast.makeText(buttonView.context, "The default port for the HTTPS protocol is 443", Toast.LENGTH_LONG).show()
                }
            }
        }

        findViewById<Button>(R.id.loginBTN).setOnClickListener {

            logIn(it);

        }

        /* lets look for some saved data */
        val data:JSONObject = retriveData();


        findViewById<EditText>(R.id.server).setText(data["server"].toString())
        findViewById<EditText>(R.id.port).setText(data["port"].toString())

        findViewById<CheckBox>(R.id.ssl).isChecked = data["ssl"].toString().toBoolean()

        findViewById<EditText>(R.id.username).setText(data["username"].toString())
        findViewById<EditText>(R.id.password).setText(data["password"].toString())

        findViewById<CheckBox>(R.id.remember).isChecked = data["remember"].toString().toBoolean()
        findViewById<CheckBox>(R.id.autologin).isChecked = data["autologin"].toString().toBoolean()

        if(data["autologin"].toString().toBoolean()){
            logIn(findViewById<Button>(R.id.loginBTN));
        }

    }

    protected fun logIn(it:View){

        LoadingDialog?.findViewById<TextView>(R.id.loading_text)?.text = "Loggin in...";

        var idata = JSONObject();

        if(getInput(idata)){
            LoadingDialog?.show();

            val api = SinusAPI(idata.get("server").toString(), port=idata.get("port").toString().toInt(), SSL=idata.get("ssl").toString().toBoolean());

            val intent = Intent(this, MainActivity::class.java);

            var url = idata["server"]
            if(idata["ssl"].toString().toBoolean()){
                url = "https://"+url
            } else {
                url = "http://"+url
            }

            GlobalScope.launch(Dispatchers.IO) {

                if(findViewById<CheckBox>(R.id.remember).isChecked){

                        storeData(
                                url,
                                idata["port"].toString(),
                                idata["ssl"].toString().toBoolean(),
                                idata["username"].toString(),
                                idata["password"].toString(),
                                idata["remember"].toString().toBoolean(),
                                idata["autologin"].toString().toBoolean()
                        )

                } else {

                    refreshKeys();

                    if(keyAliases.contains("SinusControllerLoginKey"))  deleteKey("SinusControllerLoginKey");

                    deleteData();

                }

                val res = async {api.login(idata.get("username").toString(), idata.get("password").toString())}.await();

                LoadingDialog?.dismiss();
                if(res.status == State.SUCCESS){
                    intent.putExtra("SinusbotAPI", api);
                    startActivity(intent);
                } else {
                    runOnUiThread {
                        Toast.makeText(it.context, "Login failed\n${res.payload.toString()}", Toast.LENGTH_LONG).show()
                    }
                }

            }





        }


        /*findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }*/
    }

    protected fun deleteData(){
        val sharedPref = this?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            remove("server")
            remove("port")
            remove("ssl")
            remove("username")
            remove("password")
            remove("remember")
            remove("autologin")
            apply()
        }
    }

    fun getInput(data:JSONObject):Boolean{

        var server = findViewById<EditText>(R.id.server)
        val port = findViewById<EditText>(R.id.port)
        val ssl = findViewById<CheckBox>(R.id.ssl).isChecked();

        val username = findViewById<EditText>(R.id.username);
        val password = findViewById<EditText>(R.id.password);

        val remember = findViewById<CheckBox>(R.id.remember).isChecked();
        val autologin = findViewById<CheckBox>(R.id.autologin).isChecked();

        var errorMSG = "";

        if(password.text.toString().trim().equals("")) {
            errorMSG = "Please fill in password field";
        }

        if(username.text.toString().trim().equals("")) {
            errorMSG = "Please fill in username field";
        }

        if(port.text.toString().trim().equals("")){
            errorMSG = "Please fill in port field";
        } else
            if(port.text.toString().toInt() < 1 || port.text.toString().toInt() > 65535) errorMSG = "Please fill valid port (1 - 65535)";

        if(server.text.toString().trim().equals("")){
            errorMSG = "Please fill in server field";
        } else
            if(!URLUtil.isValidUrl(server.text.toString().trim())){
                errorMSG = "Please fill valid server URL";
            }


        if(errorMSG != ""){
            runOnUiThread {
                Toast.makeText(server.context, errorMSG, Toast.LENGTH_LONG).show()
            }
            return false;
        }

        data.put("server", server.text.toString().replace("https://", "").replace("http://", ""));
        data.put("port", port.text.toString());
        data.put("ssl", ssl);
        data.put("username", username.text.toString().trim());
        data.put("password", password.text.toString().trim());
        data.put("remember", remember);
        data.put("autologin", autologin);

        return true;
    }

    //endregion

    //region Cypher tools

    protected fun retriveData():JSONObject{


        refreshKeys()
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE) ?: return JSONObject("{}");

        try {
            val ip = sharedPref.getString("server", "")
            val port = sharedPref.getString("port", "")
            val ssl = sharedPref.getBoolean("ssl", false)
            val user = sharedPref.getString("username", "")
            val pass = sharedPref.getString("password", "")
            val remember = sharedPref.getBoolean("remember", false)
            val autologin = sharedPref.getBoolean("autologin", false)

            val server = ip?.let { decryptString("SinusControllerLoginKey", it) }
            val username = user?.let { decryptString("SinusControllerLoginKey", it) }
            val password = pass?.let { decryptString("SinusControllerLoginKey", it) }

            return JSONObject(mapOf(
                    "server" to server,
                    "port" to port,
                    "ssl" to ssl,
                    "username" to username,
                    "password" to password,
                    "remember" to remember,
                    "autologin" to autologin
            ))
        } catch (e: Exception){
            // profesionální zahození chyby
        }

        return JSONObject("{}")

    }

    protected fun storeData(server:String, port:String, ssl:Boolean, username:String, password:String, remember:Boolean, autologin:Boolean):Boolean{

        refreshKeys()

        if(!keyAliases.contains("SinusControllerLoginKey"))
            createNewKeys("SinusControllerLoginKey");

        val user = encryptString("SinusControllerLoginKey", username);
        val pass = encryptString("SinusControllerLoginKey", password);
        val ip = encryptString("SinusControllerLoginKey", server);


        if(user.isEmpty() || pass.isEmpty()) return false;

        val sharedPref = this.getPreferences(Context.MODE_PRIVATE) ?: return false;
        with (sharedPref.edit()) {
            putString("server", ip)
            putString("port", port)
            putBoolean("ssl", ssl)
            putString("username", user)
            putString("password", pass)
            putBoolean("remember", remember)
            putBoolean("autologin", autologin)
            apply()
        }

        return true;
    }



    protected fun refreshKeys() {
        keyAliases = ArrayList()
        try {
            val aliases: Enumeration<String> = ks.aliases()
            while (aliases.hasMoreElements()) {
                keyAliases.add(aliases.nextElement())
            }
        } catch (e: Exception) {
        }
    }

    protected fun createNewKeys(alias:String) {
        try {
            // Create new key if needed
            if (!ks.containsAlias(alias)) {
                val start: Calendar = Calendar.getInstance()
                val end: Calendar = Calendar.getInstance()
                end.add(Calendar.YEAR, 1)
                val spec = KeyPairGeneratorSpec.Builder(this)
                        .setAlias(alias)
                        .setSubject(X500Principal("CN=Sample Name, O=Android Authority"))
                        .setSerialNumber(BigInteger.ONE)
                        .setStartDate(start.getTime())
                        .setEndDate(end.getTime())
                        .build();
                val generator: KeyPairGenerator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore")
                generator.initialize(spec)
                val keyPair: KeyPair = generator.generateKeyPair()
            }
        } catch (e: Exception) {
            // profesionální zahození chyby
        }
        refreshKeys()
    }

    protected fun deleteKey(alias: String) {
        try {
            ks.deleteEntry(alias)
            refreshKeys()
        } catch (e: KeyStoreException) {
            // profesionální zahození chyby
        }

    }

    protected fun encryptString(alias: String, initialText: String): String {
        try {
            val privateKeyEntry = ks.getEntry(alias, KeyStore.PasswordProtection("q3ItiUT94OKIq3phxatD".toCharArray())) as KeyStore.PrivateKeyEntry
            val publicKey : PublicKey = privateKeyEntry.certificate.publicKey

            // Encrypt the text

            if (initialText.isEmpty()) {
                return "";
            }
            val input: Cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding"/*, "AndroidOpenSSL"*/) // marshmallow (android 6.0) změna openSSL -> BoringSSL (pravděpodobný důvod chyby)
            input.init(Cipher.ENCRYPT_MODE, publicKey)
            val outputStream = ByteArrayOutputStream()
            val cipherOutputStream = CipherOutputStream(
                    outputStream, input)
            cipherOutputStream.write(initialText.toByteArray(charset("UTF-8")))
            cipherOutputStream.close()
            val vals: ByteArray = outputStream.toByteArray()
            return android.util.Base64.encodeToString(vals, android.util.Base64.DEFAULT)
        } catch (e: java.lang.Exception) {
            // profesionální zahození chyby
        }
        return "";
    }

    protected fun decryptString(alias: String, encryptedText: String): String {
        try {
            val privateKeyEntry = ks.getEntry(alias, KeyStore.PasswordProtection("q3ItiUT94OKIq3phxatD".toCharArray())) as KeyStore.PrivateKeyEntry
            val privateKey : PrivateKey = privateKeyEntry.privateKey
            val output = Cipher.getInstance("RSA/ECB/PKCS1Padding"/*, "AndroidOpenSSL"*/)
            output.init(Cipher.DECRYPT_MODE, privateKey)
            val cipherText: String = encryptedText
            val cipherInputStream = CipherInputStream(
                    ByteArrayInputStream(Base64.decode(cipherText, Base64.DEFAULT)), output)
            val values: ArrayList<Byte> = ArrayList()
            var nextByte: Int
            while (cipherInputStream.read().also { nextByte = it } != -1) {
                values.add(nextByte.toByte())
            }
            val bytes = ByteArray(values.size)
            for (i in bytes.indices) {
                bytes[i] = values[i]
            }
            return String(bytes, 0, bytes.size, Charset.defaultCharset())
        } catch (e: java.lang.Exception) {
            // profesionální zahození chyby
        }
        return "";
    }
//endregion

}