package tk.lukaskabc.sinusbotcontroller.ui.playlists

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import tk.lukaskabc.sinusbotcontroller.MainActivity
import tk.lukaskabc.sinusbotcontroller.R
import tk.lukaskabc.sinusbotcontroller.SinusAPI
import tk.lukaskabc.sinusbotcontroller.State
import java.lang.Exception
import kotlin.collections.ArrayList

class PlaylistsFragment : Fragment(), PlaylistsAdapter.OnItemClickListener {

    private val exampleList = ArrayList<PlaylistItem>()
    private var adapter = PlaylistsAdapter(ArrayList<PlaylistItem>(), this)
    private var lastItem: String = ""
    var api: SinusAPI? = null
    lateinit var recyclerView: RecyclerView
    var LoadingDialog1: AlertDialog? = null
    var LoadingDialog2: AlertDialog? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View?
    {
        /* Recieve api from MainActivity */
        this.api = (activity as MainActivity?)?.api
        (activity as MainActivity?)?.playlistFragment = this

        val root = inflater.inflate(R.layout.fragment_playlists, container, false)

        /* Processing request */
        val customView1: View = inflater.inflate(R.layout.loading_dialog, null)
        customView1.findViewById<TextView>(R.id.loading_text).text = "Processing request..."
        val builder1 = AlertDialog.Builder(requireContext())
        builder1.setCancelable(false)
        builder1.setView(customView1)
        LoadingDialog1 = builder1.create();

        /* Loading request */
        val customView2: View = inflater.inflate(R.layout.loading_dialog, null)
        customView2.findViewById<TextView>(R.id.loading_text).text = "Loading content..."
        val builder2 = AlertDialog.Builder(requireContext())
        builder2.setCancelable(false)
        builder2.setView(customView2)
        LoadingDialog2 = builder2.create()

        LoadingDialog2?.show()

        recyclerView = root.findViewById<RecyclerView>(R.id.ListOfPlaylists)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)

        /* Loading and setting playlists */
        GlobalScope.launch(Dispatchers.IO) {
            val res = async {api?.get("/playlists")}.await()
            if (res?.status == State.SUCCESS){
                val result: JSONArray = res?.payload as JSONArray
                for (i in 0 until result.length()) {
                    val list = result[i] as JSONObject;
                    val entries: JSONArray = list.get("entries") as JSONArray
                    GlobalScope.launch(Dispatchers.Main) {
                        exampleList.add(0, PlaylistItem(list.get("name").toString(), entries.length(), list.get("uuid").toString(), Color.rgb(255, 255, 255)))
                    }
                }
                GlobalScope.launch(Dispatchers.Main) {
                    adapter.setData(exampleList)
                    adapter.notifyDataSetChanged()
                    (activity as MainActivity?)?.updateStatus()
                    LoadingDialog2?.dismiss()
                }
            } else if (res?.status == State.FAILED){
                GlobalScope.launch(Dispatchers.Main){
                    Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                }
            }
        }
        return root
    }

    override fun onItemClick(position: Int, itemView: View) {
        LoadingDialog1?.show()
        GlobalScope.launch(Dispatchers.IO) {
            val res = async {  api!!.post("/play/byList/"+exampleList[position].uuid+"/0", mapOf() ,true)}.await()
            if(res.status == State.SUCCESS){
                (activity as MainActivity?)?.updateStatus()
            } else if (res.status == State.FAILED){
                GlobalScope.launch(Dispatchers.Main){
                    Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun statusInfo(playlistId: String){
        /* Select the currently playing playlist */
        if (playlistId != ""){
            val newitem = exampleList.find{ y -> y.uuid == playlistId }!!
            if(lastItem != ""&& lastItem != playlistId){
                Log.e("update1", "Updated "+playlistId)
                val olditem = exampleList.find{ y -> y.uuid == lastItem }!!
                GlobalScope.launch(Dispatchers.IO) {
                    activity?.runOnUiThread {
                        newitem.color = Color.rgb(255, 219, 219)
                        olditem.color = Color.rgb(255, 255, 255)
                    }
                }
                lastItem = playlistId
            }
            else if (lastItem == ""){
                Log.e("update2", "Updated "+playlistId)
                GlobalScope.launch(Dispatchers.IO) {
                    activity?.runOnUiThread {
                        newitem.color = Color.rgb(255, 219, 219)
                    }
                }
                lastItem = playlistId
            }
        }
        else {
            if(lastItem != "")
            {
                Log.e("update3", "Updated "+playlistId)
                val olditem = exampleList.find{ y -> y.uuid == lastItem }!!
                GlobalScope.launch(Dispatchers.IO) {
                    activity?.runOnUiThread {
                        olditem.color = Color.rgb(255, 255, 255)
                    }
                }
                lastItem = ""
            }
        }
        GlobalScope.launch(Dispatchers.Main){
            adapter.notifyDataSetChanged()
        }
        if(LoadingDialog1?.isShowing == true){
            LoadingDialog1?.dismiss()
        }
    }
}