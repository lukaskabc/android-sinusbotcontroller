package tk.lukaskabc.sinusbotcontroller.ui.queue

import android.graphics.Bitmap

data class QueueItem(var text: String, var uuid: String, var image: Bitmap, var color: Int) {}