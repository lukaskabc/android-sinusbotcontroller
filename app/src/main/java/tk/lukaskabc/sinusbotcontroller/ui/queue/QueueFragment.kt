package tk.lukaskabc.sinusbotcontroller.ui.queue

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import tk.lukaskabc.sinusbotcontroller.MainActivity
import tk.lukaskabc.sinusbotcontroller.R
import tk.lukaskabc.sinusbotcontroller.SinusAPI
import tk.lukaskabc.sinusbotcontroller.State

class QueueFragment : Fragment() {

    private val exampleList = ArrayList<QueueItem>()
    private var adapter = QueueAdapter(exampleList)
    var api: SinusAPI? = null;
    lateinit var recyclerView: RecyclerView
    var LoadingDialog1: AlertDialog? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        /* Recieve api from MainActivity */
        this.api = (activity as MainActivity?)!!.api
        (activity as MainActivity?)?.queueFragment = this

        val root = inflater.inflate(R.layout.fragment_queue, container, false)

        /* Loading request */
        val customView1: View = inflater.inflate(R.layout.loading_dialog, null)
        customView1.findViewById<TextView>(R.id.loading_text).text = "Loading content..."
        val builder1 = AlertDialog.Builder(requireContext())
        builder1.setCancelable(false)
        builder1.setView(customView1)
        LoadingDialog1 = builder1.create();

        LoadingDialog1?.show()

        recyclerView = root.findViewById<RecyclerView>(R.id.ListOfQueue)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)

        /* Set onClick to button */
        val button1 = root.findViewById<Button>(R.id.EnqueueBtn)
        button1.setOnClickListener {
            GlobalScope.launch(Dispatchers.IO) {
                val textedit = root.findViewById<EditText>(R.id.TextEdit)
                // Enqueue YT
                val res = async {api!!.post("event/youtube-queue", mapOf("ytURL" to textedit.text.toString(), "instanceId" to (activity as MainActivity?)?.instanceId.toString()), useSelectedInstance = true)}.await()
                if(res.status == State.SUCCESS){
                    async { delay(5000); updateQueue(); }
                    textedit.text.clear()
                }else if (res.status == State.FAILED){
                    GlobalScope.launch(Dispatchers.Main){
                        Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

        /* Loading and setting queue */
        updateQueue()
        LoadingDialog1?.dismiss()

        return root
    }

    public fun updateQueue(){
        GlobalScope.launch(Dispatchers.IO) {
            val res = async {api?.get("queue", useSelectedInstance = true)}.await()
            if(res?.status == State.SUCCESS) {
                val result: JSONArray = res?.payload as JSONArray
                exampleList.clear()
                if(result.length() != 0){
                    for (i in result.length() downTo 1 step 1) {
                        val list = result[i-1] as JSONObject
                        var bit: Bitmap = Bitmap.createBitmap(16, 9, Bitmap.Config.ARGB_8888)
                        if(list.has("thumbnail")){
                            val jpg = async {api?.getThumbnail(list.getString("thumbnail"))}.await()
                            if(jpg?.status == State.SUCCESS) {
                                bit = jpg.payload as Bitmap
                            }else if (jpg?.status == State.FAILED){
                                GlobalScope.launch(Dispatchers.Main){
                                    Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                        GlobalScope.launch(Dispatchers.Main){
                            exampleList.add(0, QueueItem(list.get("title").toString(), list.get("uuid").toString(), bit, Color.rgb(255, 255, 255)))
                        }
                    }
                }
                GlobalScope.launch(Dispatchers.Main){
                    adapter.notifyDataSetChanged()
                }
            }else if (res?.status == State.FAILED){
                GlobalScope.launch(Dispatchers.Main){
                    Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                }
            }
        }
    }


    override fun onContextItemSelected(item: MenuItem): Boolean {
        when(item.getItemId()){
            1 -> {
                // Delete from queue
                GlobalScope.launch(Dispatchers.IO) {
                    val res = async {api?.delete("queue/"+item.groupId, useSelectedInstance = true)}.await()
                    if(res?.status == State.SUCCESS){
                        updateQueue()
                    } else if (res?.status == State.FAILED){
                        GlobalScope.launch(Dispatchers.Main){
                            Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                        }
                    }
                }
                return true
            }
            else -> { return super.onContextItemSelected(item) }
        }
    }
}