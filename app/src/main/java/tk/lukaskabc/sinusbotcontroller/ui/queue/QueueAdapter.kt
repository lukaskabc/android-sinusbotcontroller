package tk.lukaskabc.sinusbotcontroller.ui.queue

import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import tk.lukaskabc.sinusbotcontroller.R

class QueueAdapter(private val data: List<QueueItem>) : RecyclerView.Adapter<QueueAdapter.ExampleViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.queue_item, parent, false)
        return ExampleViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) {
        val currentItem = data[position]
        holder.queuename.text = currentItem.text
        holder.queueimage.setImageBitmap(currentItem.image)
        holder.queuecard.setCardBackgroundColor(currentItem.color)
    }

    override fun getItemCount() = data.size

    inner class ExampleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnCreateContextMenuListener {
        val queuename: TextView = itemView.findViewById(R.id.queueName)
        val queueimage: ImageView = itemView.findViewById(R.id.queueImage)
        var queuecard: CardView = itemView.findViewById(R.id.queueCard)
        init {
            queuecard.setOnCreateContextMenuListener(this)
        }

        override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
            menu?.add(adapterPosition, 1, 0, "Delete")
        }
    }
}