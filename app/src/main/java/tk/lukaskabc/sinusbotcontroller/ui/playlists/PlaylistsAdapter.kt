package tk.lukaskabc.sinusbotcontroller.ui.playlists

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import tk.lukaskabc.sinusbotcontroller.R
import tk.lukaskabc.sinusbotcontroller.ui.music.MusicItem

class PlaylistsAdapter(private var data: List<PlaylistItem>, private val listener: OnItemClickListener) : RecyclerView.Adapter<PlaylistsAdapter.ExampleViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.playlist_item, parent, false)
        return ExampleViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) {
        val currentItem = data[position]
        holder.playlistname.text = currentItem.text
        holder.playlistcount.text = currentItem.number.toString()
        holder.playlistcard.setCardBackgroundColor(currentItem.color)
    }

    fun setData(list: List<PlaylistItem>){
        data = list
    }

    override fun getItemCount() = data.size

    inner class ExampleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val playlistname: TextView = itemView.findViewById(R.id.playlistName)
        val playlistcount: TextView = itemView.findViewById(R.id.playlistCount)
        val playlistcard: CardView = itemView.findViewById(R.id.playlistCard)
        init {
            itemView.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            val position : Int = adapterPosition
            if(position != RecyclerView.NO_POSITION)
                listener.onItemClick(position, itemView)
        }
    }
    interface OnItemClickListener {
        fun onItemClick(position: Int, itemView: View)
    }
}