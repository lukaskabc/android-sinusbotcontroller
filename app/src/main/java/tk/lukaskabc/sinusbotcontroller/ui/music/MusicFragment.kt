package tk.lukaskabc.sinusbotcontroller.ui.music

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import tk.lukaskabc.sinusbotcontroller.*
import tk.lukaskabc.sinusbotcontroller.ui.queue.QueueFragment

class MusicFragment : Fragment(), MusicAdapter.OnItemClickListener {

    private val exampleList = ArrayList<MusicItem>()
    private var adapter = MusicAdapter(ArrayList<MusicItem>(), this)
    private var lastItem: String = ""
    var api: SinusAPI? = null;
    lateinit var recyclerView: RecyclerView
    var LoadingDialog1: AlertDialog? = null
    var LoadingDialog2: AlertDialog? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        /* Recieve api from MainActivity */
        this.api = (activity as MainActivity?)!!.api
        (activity as MainActivity?)?.musicFragment = this

        val root = inflater.inflate(R.layout.fragment_music, container, false)

        /* Processing request */
        val customView1: View = inflater.inflate(R.layout.loading_dialog, null)
        customView1.findViewById<TextView>(R.id.loading_text).text = "Processing request..."
        val builder1 = AlertDialog.Builder(requireContext())
        builder1.setCancelable(false)
        builder1.setView(customView1)
        LoadingDialog1 = builder1.create()

        /* Loading request */
        val customView2: View = inflater.inflate(R.layout.loading_dialog, null)
        customView2.findViewById<TextView>(R.id.loading_text).text = "Loading content..."
        val builder2 = AlertDialog.Builder(requireContext())
        builder2.setCancelable(false)
        builder2.setView(customView2)
        LoadingDialog2 = builder2.create()

        LoadingDialog2?.show()

        recyclerView = root.findViewById<RecyclerView>(R.id.ListOfMusic)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)

        /* Loading and setting musics */
        GlobalScope.launch(Dispatchers.IO) {
            val res = async { api?.get("/files") }.await()
            if(res?.status == State.SUCCESS){
                val result: JSONArray = res.payload as JSONArray
                for (i in 0 until result.length()) {
                    val list = result[i] as JSONObject
                    if (list.getString("type") != "folder") {
                        var bit: Bitmap = Bitmap.createBitmap(16, 9, Bitmap.Config.ARGB_8888)
                        if (list.has("thumbnail")) {
                            val jpg = async { api?.getThumbnail(list.getString("thumbnail")) }.await()
                            if(jpg?.status == State.SUCCESS) {
                                bit = jpg.payload as Bitmap
                            }else if (jpg?.status == State.FAILED){
                                GlobalScope.launch(Dispatchers.Main){
                                    Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                        GlobalScope.launch(Dispatchers.Main) {
                            try {
                                exampleList.add(0, MusicItem(list.get("title").toString(), list.get("uuid").toString(), bit, Color.rgb(255, 255, 255)))
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
                GlobalScope.launch(Dispatchers.Main) {
                    adapter.setData(exampleList)
                    adapter.notifyDataSetChanged()
                    (activity as MainActivity?)?.updateStatus()
                    LoadingDialog2?.dismiss()
                }
            } else if (res?.status == State.FAILED){
                GlobalScope.launch(Dispatchers.Main){
                    Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                }
            }
        }
        return root
    }

    override fun onItemClick(position: Int, itemView: View) {
        LoadingDialog1?.show()
        GlobalScope.launch(Dispatchers.IO) {
            val res = async {  api!!.post("/play/byId/"+exampleList[position].uuid, mapOf() ,true)}.await()
            if(res.status == State.SUCCESS){
                (activity as MainActivity?)?.updateStatus()
            } else if (res.status == State.FAILED){
                GlobalScope.launch(Dispatchers.Main){
                    Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when(item.getItemId()){
            1 -> {
                // enqueue
                GlobalScope.launch(Dispatchers.IO) {
                    val res = async {  api!!.post("/queue/append/"+exampleList[item.groupId].uuid, mapOf() ,true)}.await()
                    if (res.status == State.FAILED){
                        GlobalScope.launch(Dispatchers.Main){
                            Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                        }
                    }
                }
                return true
            }
            2 -> {
                // enqueue next
                GlobalScope.launch(Dispatchers.IO) {
                    val res = async {  api!!.post("/queue/prepend/"+exampleList[item.groupId].uuid, mapOf() ,true)}.await()
                    if (res.status == State.FAILED){
                        GlobalScope.launch(Dispatchers.Main){
                            Toast.makeText(context, "Something went wrong: "+res?.payload.toString(),Toast.LENGTH_LONG).show()
                        }
                    }
                }
                return true
            }
            else -> { return super.onContextItemSelected(item) }
        }
    }

    fun statusInfo(musicId: String){
        /* Select the currently playing music */
        if (musicId != ""){
            val newitem = exampleList.find{ y -> y.uuid == musicId }!!
            if(lastItem != "" && lastItem !=musicId){
                Log.e("update1", "Updated "+musicId)
                val olditem = exampleList.find{ y -> y.uuid == lastItem }!!
                GlobalScope.launch(Dispatchers.IO) {
                    activity?.runOnUiThread {
                        newitem.color = Color.rgb(255, 219, 219)
                        olditem.color = Color.rgb(255, 255, 255)
                    }
                }
                lastItem = musicId
            }
            else if (lastItem == ""){
                Log.e("update2", "Updated "+musicId)
                GlobalScope.launch(Dispatchers.IO) {
                    activity?.runOnUiThread {
                        newitem.color = Color.rgb(255, 219, 219)
                    }
                }
                lastItem = musicId
            }
        }
        else {
            if(lastItem != "")
            {
                Log.e("update3", "Updated "+musicId)
                val olditem = exampleList.find{ y -> y.uuid == lastItem }!!
                GlobalScope.launch(Dispatchers.IO) {
                    activity?.runOnUiThread {
                        olditem.color = Color.rgb(255, 255, 255)
                    }
                }
                lastItem = ""
            }
        }
        GlobalScope.launch(Dispatchers.Main){
            adapter.notifyDataSetChanged()
        }
        if(LoadingDialog1?.isShowing == true){
            LoadingDialog1?.dismiss()
        }
    }
}