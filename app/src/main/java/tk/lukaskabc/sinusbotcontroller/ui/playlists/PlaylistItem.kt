package tk.lukaskabc.sinusbotcontroller.ui.playlists

data class PlaylistItem(var text: String, var number: Int, var uuid: String, var color: Int) {}