package tk.lukaskabc.sinusbotcontroller.ui.music

import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import tk.lukaskabc.sinusbotcontroller.R

class MusicAdapter(private var data: List<MusicItem>, private val listener: OnItemClickListener) : RecyclerView.Adapter<MusicAdapter.ExampleViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.music_item, parent, false)
        return ExampleViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) {
        val currentItem = data[position]
        holder.musicname.text = currentItem.text
        holder.musicimage.setImageBitmap(currentItem.image)
        holder.musiccard.setCardBackgroundColor(currentItem.color)
    }

    fun setData(list: List<MusicItem>){
        data = list
    }

    override fun getItemCount() = data.size

    inner class ExampleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnCreateContextMenuListener {
        val musicname: TextView = itemView.findViewById(R.id.musicName)
        val musicimage: ImageView = itemView.findViewById(R.id.musicImage)
        var musiccard: CardView = itemView.findViewById(R.id.musicCard)
        init {
            itemView.setOnClickListener(this)
            musiccard.setOnCreateContextMenuListener(this)
        }

        override fun onClick(v: View?) {
            val position : Int = adapterPosition
            if(position != RecyclerView.NO_POSITION)
                listener.onItemClick(position, itemView)
        }

        override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
            menu?.add(adapterPosition, 1, 0, "Enqueue")
            menu?.add(adapterPosition, 2, 1, "Enqueue next")

        }
    }
    interface OnItemClickListener {
        fun onItemClick(position: Int, itemView: View)
    }

}


