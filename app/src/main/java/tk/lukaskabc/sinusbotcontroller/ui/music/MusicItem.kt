package tk.lukaskabc.sinusbotcontroller.ui.music

import android.graphics.Bitmap

data class MusicItem(var text: String, var uuid: String, var image: Bitmap, var color: Int) {}