package tk.lukaskabc.sinusbotcontroller

import org.json.JSONObject

data class SinusInstance (
    val beckend: String,
    val uuid: String,
    val name: String,
    val nick: String,
    val running: Boolean,
    val playing: Boolean,
    val mainInstance: Boolean,
    val licenseId: String,
    val serverHost: String,
    val serverPort: Int,
    val privileges: JSONObject
)