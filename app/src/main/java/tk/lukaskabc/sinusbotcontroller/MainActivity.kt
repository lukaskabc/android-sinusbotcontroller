package tk.lukaskabc.sinusbotcontroller

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ImageSpan
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import tk.lukaskabc.sinusbotcontroller.ui.music.MusicFragment
import tk.lukaskabc.sinusbotcontroller.ui.playlists.PlaylistsFragment
import tk.lukaskabc.sinusbotcontroller.ui.queue.QueueFragment
import java.util.concurrent.TimeUnit


class MainActivity() : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    var api:SinusAPI? = null;

    var context : Context? = null;

    var instanceId: String? = null;

    var sortedUUIDs = ArrayList<String>()

    init {
        context = this;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        /* Získání přihlášené api z login aktivity */
        api = intent.getSerializableExtra("SinusbotAPI") as SinusAPI?

        api?.activity = this

        if(api?.loggedIn()!!){

            runOnUiThread {
                Toast.makeText(this, "Logged in", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Login failed", Toast.LENGTH_SHORT).show()
            this.finish()
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_music, R.id.nav_queue, R.id.nav_playlists), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        val clpsbtn = findViewById<ImageButton>(R.id.controlBarCollapseBTN);
        val bar = findViewById<ConstraintLayout>(R.id.controlBar);


        clpsbtn.setOnClickListener {
            if(bar.visibility == View.VISIBLE){
                it.animate().translationY(bar.height.toFloat())
                bar.animate().translationY(bar.height.toFloat()).withEndAction {
                    bar.visibility = View.INVISIBLE
                    clpsbtn.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
                }
            } else {
                bar.visibility = View.VISIBLE

                it.animate().translationY(0F)
                bar.animate().translationY(0F).withEndAction {
                    clpsbtn.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
                }
            }
        }

        /* enabling control bar title scroll */
        findViewById<TextView>(R.id.controlBar_songText).isSelected = true


        /* instance spinner */
        val header = navView.getHeaderView(0)
        val spinner = header.findViewById<Spinner>(R.id.instanceSpinner)

        GlobalScope.launch(Dispatchers.IO) {

            val iresult = async {api!!.getInstances()}.await()

            if(iresult?.status == State.FAILED){
                runOnUiThread {
                    Toast.makeText(context, "Error geting instances", Toast.LENGTH_LONG).show();
                }
            } else {

                runOnUiThread {
                    /* načtení názvů instancí do spinneru - seřazených podle uuid */

                    var instances = iresult.payload as JSONArray
                    var instance = JSONArray();

                    sortedUUIDs = ArrayList<String>()

                    for (i in 0 until instances.length()) {
                        val inst = instances.getJSONObject(i)
                        sortedUUIDs.add(inst.get("uuid").toString())
                    }


                    sortedUUIDs.sortWith(compareBy { it });


                    for (i in 0 until instances.length()) {
                        val inst = instances.getJSONObject(i)
                        instance.put(sortedUUIDs.indexOf(inst.getString("uuid")), (inst.getString("nick")))
                    }

                    var instanceList = ArrayList<String>()

                    for (i in 0 until instance.length()) {

                        instanceList.add(instance[i].toString());

                    }

                    val adp = context?.let { ArrayAdapter<String>(it, R.layout.spinner_item_white, instanceList) }

                    spinner.adapter = adp;

                }
            }


            header.findViewById<Spinner>(R.id.instanceSpinner).onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    if(sortedUUIDs.size > position) instanceId = sortedUUIDs[position]

                    GlobalScope.launch(Dispatchers.IO) { async { updateStatus() } }

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    // sometimes you need nothing here
                }
            }

        }

        // song progress bar handler
        findViewById<SeekBar>(R.id.song_progress).setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                //useless
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                GlobalScope.launch(Dispatchers.IO) {
                    async {
                        api?.post("seek/${seekBar.progress}", mapOf(), true)
                        updateStatus()
                        seeking = false;
                    }
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // useless
                seeking = true;
            }
        })

        findViewById<ImageButton>(R.id.play).setOnClickListener {
            Log.e("play", lastPlaylist.toString() + lastPlaylistTrack.toString())

            if(PlayingState){
                // Stop
                GlobalScope.launch(Dispatchers.IO) {
                    async {
                        api?.post("stop", mapOf(), true)
                        updateStatus()
                    }
                }
            } else {
                GlobalScope.launch(Dispatchers.IO) {
                try {
                    // Play

                        if (lastTrack != "")
                            async {
                                api?.post("play/byId/$lastTrack", mapOf(), true)
                                updateStatus()
                            }
                        else if (lastPlaylist != "" && lastPlaylistTrack != null) async {
                            async { api?.post("play/byList/$lastPlaylist/$lastPlaylistTrack", mapOf(), true) }
                            updateStatus()
                        } else {
                            runOnUiThread {
                                Toast.makeText(
                                    this@MainActivity,
                                    "Unable to play this track",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    } catch (e: Exception) { e.printStackTrace()}
                }
            }
        }

        findViewById<ImageButton>(R.id.repeat).setOnClickListener {
            GlobalScope.launch(Dispatchers.IO) {
                async {
                    if(repeat) api?.post("repeat/0", mapOf(), true)
                        else api?.post("repeat/1", mapOf(), true)

                    repeat = !repeat

                    updateStatus()
                }
            }
        }

        findViewById<ImageButton>(R.id.shuffle).setOnClickListener {
            GlobalScope.launch(Dispatchers.IO) {
                async {
                    if(shuffle) api?.post("shuffle/0", mapOf(), true)
                    else api?.post("shuffle/1", mapOf(), true)

                    shuffle = !shuffle

                    updateStatus()
                }
            }
        }

        findViewById<ImageButton>(R.id.next).setOnClickListener {
            GlobalScope.launch(Dispatchers.IO) {
                async {
                    api?.post("playNext", mapOf(), true)
                    updateStatus()
                }
            }
        }

        findViewById<ImageButton>(R.id.prev).setOnClickListener {
            GlobalScope.launch(Dispatchers.IO) {
                async {
                    api?.post("playPrevious", mapOf(), true)
                    updateStatus()
                }
            }
        }

    }

    var repeat = false;
    var shuffle = false;
    var seeking = false;


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        //menuInflater.inflate(R.menu.main, menu)
        menu.add(0, 10, 1, getDrawable(R.drawable.ic_baseline_refresh_24)?.let { menuIconWithText(it, "Force refresh") });
        menu.add(0, 11, 2, getDrawable(R.drawable.ic_baseline_power_settings_new_24)?.let { menuIconWithText(it, "Log out") });
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            10 -> {
                forceRefresh()
                return true
            }
            11 -> {
                logout()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun forceRefresh() {


        val inflater = layoutInflater
        val customView: View = inflater.inflate(R.layout.loading_dialog, null)
        customView.findViewById<TextView>(R.id.loading_text).text = "Getting status...";
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setView(customView)
        var LoadingDialog: AlertDialog = builder.create();

        LoadingDialog.findViewById<TextView>(R.id.loading_text)?.text = "Getting status...";

        LoadingDialog.show();


        // just copy of update status
        updateStatus(LoadingDialog)

    }

    fun logout() {
        api?.logout();
        this.finish();
    }

    private fun menuIconWithText(r: Drawable, title: String): CharSequence? {
        r.setBounds(0, 0, r.intrinsicWidth, r.intrinsicHeight)
        val sb = SpannableString("    $title")
        val imageSpan = ImageSpan(r, ImageSpan.ALIGN_BOTTOM)
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return sb
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    var trackPosition:Long = 0
    var trackLength:Long = 0
    var lastTrack = "";
    var lastPlaylist = "";
    var lastPlaylistTrack: Int? = null

    /* status updater */

    var handler: Handler = Handler()
    var runnable: Runnable? = null;
    var delay = 10000


    var secHandler: Handler = Handler()
    var secRunnable: Runnable? = null;
    var secTimerRunning = false;

    fun updateSec() {

        trackPosition += 1000

        if(trackLength <= trackPosition && trackLength != 0.toLong()) {
            PlayingState = false
            checkSecTimer()
            trackPosition = trackLength
            updateStatus()
        }

        findViewById<TextView>(R.id.controlbar_songTime).text = formatTime(trackPosition)

        var percentPos:Long = 0
        if(trackLength > 0) percentPos = trackPosition / (trackLength/100)
        if(!seeking) findViewById<SeekBar>(R.id.song_progress).progress = percentPos.toInt()
    }

    fun checkSecTimer() {
        if(PlayingState){
            if(!secTimerRunning){
                secTimerRunning = true
                secHandler?.postDelayed(Runnable {
                    updateSec()
                    secRunnable?.let { secHandler?.postDelayed(it, 1000) }
                }.also { secRunnable = it }, 1000)
            }

        } else {
            secTimerRunning = false
            secRunnable?.let { secHandler?.removeCallbacks(it) }
        }
    }

    var firstSuccessStatus = false;

    protected override fun onResume(): Unit {

        if(firstSuccessStatus)GlobalScope.launch(Dispatchers.IO) { async { updateStatus() } }

        handler?.postDelayed(Runnable {
            updateStatus()
            runnable?.let { handler?.postDelayed(it, delay.toLong()) }
        }.also { runnable = it }, delay.toLong())

        checkSecTimer()

        super.onResume()
    }

    override fun onPause() {
        runnable?.let { handler?.removeCallbacks(it) } //stop handler when activity not visible super.onPause();
        secRunnable?.let { secHandler?.removeCallbacks(it) }
        secTimerRunning = false
        super.onPause()
    }

    var playlistFragment : PlaylistsFragment? = null;
    var musicFragment : MusicFragment? = null;
    var queueFragment :  QueueFragment? = null

    var PlayingState = false;

    fun updateStatus(alertdialog: AlertDialog? = null) {

        GlobalScope.launch(Dispatchers.IO) {
            try {
                val rstatus = async { api!!.get("status", useSelectedInstance = true) }.await()
                if (rstatus.status == State.FAILED) {
                    runOnUiThread {
                        Toast.makeText(context, "Getting status failed", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    firstSuccessStatus = true;

                    val status = rstatus.payload as JSONObject

                    val fm: FragmentManager = supportFragmentManager
                    val entries: JSONObject = status.get("currentTrack") as JSONObject

                    // pass playlist to playlists fragment
                    /*if (status.getString("playlist") != "") {
                        try {
                            playlistFragment?.statusInfo(status.getString("playlist"))
                            queueFragment?.updateQueue()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else if (status.getString("playlist") == "") {
                        Log.e("test", status.toString())
                        if (entries.getString("uuid") != "" && entries.getString("type") != "temp") {
                            try {
                                musicFragment?.statusInfo(entries.getString("uuid"))
                                queueFragment?.updateQueue()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        } else if (entries.getString("uuid") == "") {
                            try {
                                queueFragment?.updateQueue()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }*/
                    try{
                        playlistFragment?.statusInfo(status.getString("playlist"))
                        musicFragment?.statusInfo(entries.getString("uuid"))
                        queueFragment?.updateQueue()
                    } catch (e:Exception) { e.printStackTrace() }

                    //get some data
                    PlayingState = status.getBoolean("playing")

                    checkSecTimer()

                    trackPosition = status.getLong("position")
                    shuffle = status.getBoolean("shuffle")
                    repeat = status.getBoolean("repeat")

                    val track = status.getJSONObject("currentTrack")

                    if (track.has("uuid")) lastTrack = track.getString("uuid")
                    else lastTrack = ""

                    if (status.has("playlist")) if (status.getString("playlist") != "") lastPlaylist = status.getString("playlist")

                    if (status.has("playlistTrack")) lastPlaylistTrack = status.getString("playlistTrack").toInt()

                    trackLength = 0

                    if (track.has("duration")) trackLength = track.getLong("duration");


                    var percentPos: Long = 0
                    if (trackLength > 0) percentPos = trackPosition / (trackLength / 100)



                    runOnUiThread {
                        //set play/stop button icon
                        if (PlayingState) findViewById<ImageButton>(R.id.play).setImageResource(R.drawable.ic_baseline_stop_24)
                        else findViewById<ImageButton>(R.id.play).setImageResource(R.drawable.ic_baseline_play_arrow_24)

                        // shuffle button
                        if (shuffle) findViewById<ImageButton>(R.id.shuffle).setColorFilter(ContextCompat.getColor(this@MainActivity, R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);
                        else findViewById<ImageButton>(R.id.shuffle).setColorFilter(ContextCompat.getColor(this@MainActivity, R.color.another_black), android.graphics.PorterDuff.Mode.SRC_IN);

                        // repeat button
                        if (repeat) findViewById<ImageButton>(R.id.repeat).setColorFilter(ContextCompat.getColor(this@MainActivity, R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);
                        else findViewById<ImageButton>(R.id.repeat).setColorFilter(ContextCompat.getColor(this@MainActivity, R.color.another_black), android.graphics.PorterDuff.Mode.SRC_IN);

                        //time text
                        findViewById<TextView>(R.id.controlbar_songTime).text = formatTime(trackPosition)
                        findViewById<TextView>(R.id.controlbar_songFullTime).text = formatTime(trackLength)

                        //Seek bar
                        if (!seeking) findViewById<SeekBar>(R.id.song_progress).progress = percentPos.toInt()

                        // set control bar song title
                        val txttitle = findViewById<TextView>(R.id.controlBar_songText);
                        val newText = (status.get("currentTrack") as JSONObject).get("title").toString()
                        if (txttitle.text != newText) txttitle.text = newText

                        alertdialog?.hide()
                        alertdialog?.dismiss();

                    }

                }
            } catch(e: Exception) {e.printStackTrace()}
        }
    }

    fun formatTime(millis:Long):String {
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1)).replace(Regex("^00:"), "")
    }

}


